// tailwind.config.js
const colors = require('tailwindcss/colors')

module.exports = {
  purge: {
    enabled: !process.env.ROLLUP_WATCH,
    content: ['./public/index.html', './src/**/*.svelte'],
    options: {
      defaultExtractor: content => [
        ...(content.match(/[^<>"'`\s]*[^<>"'`\s:]/g) || []),
        ...(content.match(/(?<=class:)[^=>\/\s]*/g) || []),
      ],
    },
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      sans: ['Ubuntu', 'sans-serif']
    },
    colors: {
      gray: colors.trueGray,
      green: colors.emerald,
      blue: colors.cyan,
      red: colors.rose,
      indigo: colors.violet,
      white: colors.white,
      black: colors.black
    },
    extend: {
      typography(theme) {
        return {
          DEFAULT: {
            css: {
              color: theme("colors.gray.300"),
              '[class~="lead"]': {
                color: theme("colors.gray.400")
              },
              a: {
                color: theme("colors.gray.100")
              },
              strong: {
                color: theme("colors.gray.100")
              },
              "ul > li::before": {
                backgroundColor: theme("colors.gray.700")
              },
              hr: {
                borderColor: theme("colors.gray.800")
              },
              blockquote: {
                color: theme("colors.gray.100"),
                borderLeftColor: theme("colors.gray.800"),
              },
              h1: {
                color: theme("colors.gray.100")
              },
              h2: {
                color: theme("colors.gray.100")
              },
              h3: {
                color: theme("colors.gray.100")
              },
              h4: {
                color: theme("colors.gray.100")
              },
              code: {
                color: theme("colors.gray.100")
              },
              "a code": {
                color: theme("colors.gray.100")
              },
              pre: {
                color: theme("colors.gray.200"),
                backgroundColor: theme("colors.gray.800"),
              },
              thead: {
                color: theme("colors.gray.100"),
                borderBottomColor: theme("colors.gray.700"),
              },
              "tbody tr": {
                borderBottomColor: theme("colors.gray.800")
              },
            },
          },
        }
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/typography')
  ],
}