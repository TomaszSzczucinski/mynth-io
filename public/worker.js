onmessage = (event) => {
  importScripts('https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/highlight.min.js');
  const result = self.hljs.highlightAuto(event.data);
  postMessage(result.value);
};