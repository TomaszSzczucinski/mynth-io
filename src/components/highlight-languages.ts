export default [
  {
    name: 'Markup',
    alias: 'markup'
  },
  {
    name: 'CSS',
    alias: 'css'
  },
  {
    name: 'HTML',
    alias: 'html'
  },
  {
    name: 'Apache Configuration',
    alias: 'apacheconf'
  },
  {
    name: 'Arduino',
    alias: 'arduino'
  },
  {
    name: 'Bash',
    alias: 'bash'
  },
  {
    name: 'C',
    alias: 'c'
  },
  {
    name: 'C++',
    alias: 'cpp'
  },
  {
    name: 'C#',
    alias: 'cs'
  },
  {
    name: 'Elixir',
    alias: 'elixir'
  },
  {
    name: 'Git',
    alias: 'git'
  },
  {
    name: 'Pascal',
    alias: 'pascal'
  },
  {
    name: 'nginx',
    alias: 'nginx'
  },
  {
    name: 'Go',
    alias: 'go'
  },
  {
    name: 'GraphQL',
    alias: 'graphql'
  },
  {
    name: 'Java',
    alias: 'java'
  },
  {
    name: 'JSON',
    alias: 'json'
  },
  {
    name: 'Julia',
    alias: 'julia'
  },
  {
    name: 'Kotlin',
    alias: 'kotlin'
  },
  {
    name: 'PHP',
    alias: 'php'
  },
  {
    name: 'YAML',
    alias: 'yaml'
  },
  {
    name: 'TypeScript',
    alias: 'ts'
  },
  {
    name: 'SQL',
    alias: 'sql'
  },
  {
    name: 'Swift',
    alias: 'swift'
  },
  {
    name: 'Sass',
    alias: 'sass'
  },
  {
    name: 'Scss',
    alias: 'scss'
  },
  {
    name: 'Rust',
    alias: 'rust'
  },
  {
    name: 'Regex',
    alias: 'regex'
  },
  {
    name: 'Python',
    alias: 'python'
  },
  {
    name: 'React TSX',
    alias: 'tsx'
  },
  {
    name: 'React JSX',
    alias: 'jsx'
  },
  {
    name: 'JavaScript',
    alias: 'js'
  }
]