import { writable } from 'svelte/store'
import config from './config'

interface InterfaceCache {
  id: string
  to: Date
  fsos: any
}

interface InterfaceFSOS {
  path: string
  opts: {
    body?: any
    method?: string
    auth?: boolean
    noHeaders?: boolean
  }
  data: any
  status: 'loading' | 'loaded' | 'error'
}

class Fetcher {
  private static instance: Fetcher
  private cache: InterfaceCache[]

  private constructor() {
    this.cache = []
  }

  public static getInstance(): Fetcher {
    if (!Fetcher.instance) {
      Fetcher.instance = new Fetcher()
    }

    return Fetcher.instance
  }

  /**
   * Logic
   */
  public fetcher = (
    path: string,
    opts?: {
      body?: any
      method?: string
      auth?: boolean
      noHeaders?: boolean
    }
  ) => {
    const cached: InterfaceCache | null = this.cache.find(
      (i: InterfaceCache) => i.id === path
    )

    if (cached) return cached.fsos

    const fsos = fetcherFSOS(path, opts)

    // fsos.get()

    this.cache.push({
      id: path,
      fsos: fsos,
      to: new Date()
    })

    return fsos
  }

  public update = async (
    path: string
  ) => {
    const cached: InterfaceCache | null = this.cache.find(
      (i: InterfaceCache) => i.id === path
    )
    
    if (cached) await cached.fsos.refresh()
  }

  public remove = async (
    path: string
  ) => {
    this.cache.filter(
      (i: InterfaceCache) => i.id === path
    )
    console.log(this.cache)
  }

  private refreshToken = async (): Promise<boolean> => {
    const r = await fetch(`${config.base_path}refreshToken`, {
      credentials: 'include'
    })
    return r.ok
  }

  private _fetch = async (path: string, opts?: any): Promise<any> => {
    return new Promise((resolve, reject) => {
      fetch(`${config.base_path}${path}`, {
        method: opts?.method ? opts.method : 'GET',
        credentials: opts?.auth ? 'include' : 'omit',
        headers: opts?.noHeaders
          ? undefined
          : {
              'Content-Type': 'application/json'
            },
        body: opts?.body ? opts.body : null
      }).then(r => resolve(r)).catch(e => reject(e))
    })
  }

  public fetch = async (
    path: string,
    opts?: {
      body?: any
      method?: string
      auth?: boolean
      noHeaders?: boolean
    }
  ): Promise<any> => {
    // console.log('[FETCHER] ==>', path, _fetch)
    console.log('[FETCHER] ==>', path)

    if (opts && opts.body && opts.noHeaders !== true) opts.body = JSON.stringify(opts.body)

    // let r = await _fetch
    let r: Response = await this._fetch(path, opts)

    if (!r.ok && r.status === 401) 
    {
      await this.refreshToken()
      r = await this._fetch(path, opts)
    }

    return await r.json()
  }
}

const fetcherFSOS = (path: string, opts: any) => {
  const { subscribe, set, update } = writable<InterfaceFSOS>({
    path: path,
    opts: opts,
    data: null,
    status: 'loading'
  })

  const refresh = () => {
    Fetcher.getInstance().fetch(path, opts).then(r => {
      update(storeState => {
        storeState.data = r
        if (r && r.success)
        {
          storeState.status = 'loaded'
        }
        else {
          storeState.status = 'error'
        }

        return storeState
      })
    }).catch(e => {
      update(storeState => {
        storeState.status = 'error'
        return storeState
      })
    })
  }

  refresh()

  return {
    subscribe,
    refresh,
    set,
    update,
    clear: () => set(null)
  }
}

// initialize the store
// const fsos = createFSOS()
export default Fetcher.getInstance()
