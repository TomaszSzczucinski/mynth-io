export const mFetch = async (path, opts: any = {}) => {

  if (!opts.headers) opts = {...opts, headers: {'Content-Type': 'application/json'}}

  let response = await fetch('http://192.168.1.100:3000/' + path, {
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json'
    },
    ...opts
  }).catch((e) => {
    return e
  })

  console.log(response)

  if (response.status && response.status === 401) {
    const refreshResponse = await fetch(
      'http://192.168.1.100:3000/refreshToken',
      {
        headers: {
          'Content-Type': 'application/json'
        },
        credentials: 'include'
      }
    )

    if (refreshResponse.ok) {
      response = await fetch('http://192.168.1.100:3000/' + path, {
        headers: {
          'Content-Type': 'application/json'
        },
        credentials: 'include',
        ...opts
      })
    }
  }

  if (response && response.ok) {
    return response.json()
  }

  return null
}
