import { writable } from 'svelte/store'

// constructor function
const createStore = () => {
  // initialize internal writable store with empty list
  const { subscribe, set, update } = writable<number[]>([])

  const add = (like: number) => {
    update(data => [...data, like])
  }

  const remove = (like: number) => {
    update(data => {
      data.splice(data.indexOf(like), 1)
      return data
    })
  }

  return {
    subscribe,
    add,
    remove,
    init: (data) => set(data),
    clear: () => set([])
  }
}

// initialize the store
const postLikes = createStore()

export default postLikes
