import { writable } from 'svelte/store'
import { mFetch } from '../services/mFetch'

// constructor function
const createStore = () => {
  // initialize internal writable store with empty list
  const { subscribe, set, update } = writable<any>(null)

  const refresh = () => {
    mFetch('profile').then(data => {
      if (data) set(data.profile)
    })
  }

  return {
    subscribe,
    refresh,
    init: (data) => set(data),
    clear: () => set(null)
  }
}

// initialize the store
const myProfile = createStore()

export default myProfile
