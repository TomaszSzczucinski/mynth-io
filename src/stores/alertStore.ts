import { writable } from 'svelte/store'

// constructor function
const createStore = () => {
  // initialize internal writable store with empty list
  const { subscribe, set, update } = writable<string[]>([])

  const add = (notification: string) => {
    update(data => [...data, notification])
    setTimeout(() => {
      remove(notification)
    }, 5000)
  }

  const remove = (notification: string) => {
    update(data => {
      data.splice(data.indexOf(notification), 1)
      return data
    })
  }

  return {
    subscribe,
    add,
    remove,
    clear: () => set([])
  }
}

// initialize the store
const alerts = createStore()

export default alerts